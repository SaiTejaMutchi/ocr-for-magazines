# About Project
I'm enthusiastic learner. I tried OCR Tesseract to read the image and extract the contained text.

I have used C++.This is a basic project which can detect text from images.It may not work for all images.For that we have to process the image before.

For now this project can be used to detect text from Photo of a paragraph or Simple Number plate. On further adding image processing to input image we can improve.

CMake for compilation
It almost became an industry standard that we should create a separate folder to store the compilation result.

The main program is in main.cpp


## Input images:

1. ![](https://gitlab.com/SaiTejaMutchi/ocr-for-magazines/-/raw/master/images1/3.jpeg)
2. ![](https://gitlab.com/SaiTejaMutchi/ocr-for-magazines/-/raw/master/images1/1.png)

## Output:
![](https://gitlab.com/SaiTejaMutchi/ocr-for-magazines/-/raw/master/Output/output.png)
