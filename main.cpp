// Add tesseract, leptonica to your system and compile through build 

#include <fstream>
#include <iostream>
#include <string>
#include <filesystem>
#include <chrono>
#include <leptonica/allheaders.h>
#include <tesseract/baseapi.h>
#include <opencv2/opencv.hpp>





using namespace std;
using namespace cv;

int main() {

 	string imagePath="../images1" ;       // given input folder images2(containing test images) as string
 	Mat image;

// Applying tesseract

tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
  api->Init(NULL, "eng", tesseract::OEM_LSTM_ONLY);
  api->SetPageSegMode(tesseract::PSM_AUTO);
  api->SetVariable("debug_file", "tesseract.log");

    for (const auto &fn : std::filesystem::directory_iterator(imagePath)) {
    	    auto start = std::chrono::steady_clock::now();
    		auto filepath = fn.path();
    		std::cout << "Detecting text in " << filepath << std::endl;



    	image=imread(filepath,1);

      //Image Processing       remove comment to apply image processing

    	// resize(image,image,Size(image.cols*2, image.rows*2));

      //cvtColor(image,image, COLOR_BGR2GRAY);

      //GaussianBlur(image, image, Size(1, 1), 0);

     

    	api->SetImage(image.data, image.cols, image.rows, 3, image.step);

    	std::string outText = api->GetUTF8Text();
    	std::cout << outText << std::endl; // outputing the text recognised

}
    
    api->End();


}